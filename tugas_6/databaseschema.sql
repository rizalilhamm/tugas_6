USE CustomerComplaintDB;

CREATE TABLE `customer_complaint` (
  `complaintID` int(30) NOT NULL,
  `data_received` date DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `sub_product` varchar(100) DEFAULT NULL,
  `issue` varchar(100) DEFAULT NULL,
  `sub_issue` varchar(100) DEFAULT NULL,
  `customer_complaint_narrative` longtext,
  `company_public_response` longtext,
  `company` varchar(100) DEFAULT NULL,
  `state_name` char(5) DEFAULT NULL,
  `zip_code` varchar(100) DEFAULT NULL,
  `tags` varchar(100) DEFAULT NULL,
  `consumer_consent_provided` enum('N/A','Consent not provided','Other','Consent provided','') NOT NULL DEFAULT '',
  `submitted_via` enum('Web','Referral','Postal mail','Fax','Phone','Email','') NOT NULL DEFAULT '',
  `date_sent_to_company` date DEFAULT NULL,
  `company_response_to_customer`
      enum('Closed with explanation','Closed with monetary relief','Closed with non-monetary relief','Closed','Untimely response')
      NOT NULL DEFAULT 'Untimely response',
  `timely_response` enum('Yes','No') NOT NULL DEFAULT 'No',
  `customer_disputed` enum('Yes','No','') NOT NULL DEFAULT '',
  PRIMARY KEY (`complaintID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1