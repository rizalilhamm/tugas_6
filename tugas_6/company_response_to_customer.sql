
USE CustomerComplaintDB;

SELECT DISTINCT company,
	SUM(CASE WHEN company_response_to_customer='Closed' THEN 1 ELSE 0 END) as closed_response,
	SUM(CASE WHEN company_response_to_customer='Closed with explanation' THEN 1 ELSE 0 END) as closed_with_explanation,
	SUM(CASE WHEN company_response_to_customer='Closed with monetory relief' THEN 1 ELSE 0 END) as closed_with_monetory_relief,
	SUM(CASE WHEN company_response_to_customer='Closed with non-monetory relief' THEN 1 ELSE 0 END) as closed_non_monetory_relief,
	SUM(CASE WHEN company_response_to_customer='Untimely response' THEN 1 ELSE 0 END) as untimely_response
FROM customer_complaint
GROUP BY company;